```plantuml
@startuml

class Score {
  - score: int
  + ScoreChanged: Action<int>
  + CurrentScore: int
}

class ScoreUI {
  - scoreText: Text
}

Score --> ScoreUI: ScoreChanged

@enduml
