using UnityEngine;

public abstract class RaycastWeapon : MonoBehaviour
{
    [SerializeField] private Transform _cameraTransform;

    [SerializeField] private float _rayRange;
    [SerializeField] private GameObject targethitbox;

    protected void PerformShot()
    {
        var cameraForward = _cameraTransform.forward;
        RaycastHit hit;
        if(Physics.Raycast(_cameraTransform.position, cameraForward, out hit, _rayRange))
        {
            ScanHit(hit);
        }
    }

    private void ScanHit(RaycastHit hit)
    {
        var colliderGameObject = hit.collider.gameObject;

        if (colliderGameObject.TryGetComponent(out Hit hittarget))
        {
            hittarget.Accept(new TargetHitBox());
        }
    }
}
