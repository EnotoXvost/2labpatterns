using UnityEngine;
using System;

public class TargetHitBox : ITargetVisitor
{
    [SerializeField] private int _scoreFromBody = 10;
    [SerializeField] private int _scoreFromHead = 20;

    public static event Action<int> OnHit;

    public void Visit(Body hit)
    {
        PerformAction(_scoreFromBody);
    }

    public void Visit(Head hit)
    {
        PerformAction(_scoreFromHead);
    }

    public static void PerformAction(int TypeOfHit)
    {
        OnHit?.Invoke(TypeOfHit);
    }
}
