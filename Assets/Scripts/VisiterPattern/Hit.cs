
using UnityEngine;

public abstract class Hit : MonoBehaviour
{
    public abstract void Accept(ITargetVisitor visitor);
}
