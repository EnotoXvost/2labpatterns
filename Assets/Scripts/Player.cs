using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject _pistol;

    private Pistol pistol;

    private void Awake()
    {
        pistol = _pistol.GetComponent<Pistol>();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            pistol.Shoot();
        }
    }
}
