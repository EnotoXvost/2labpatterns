using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreUI : MonoBehaviour
{
    [SerializeField] private Text _scoreText;
    [SerializeField] private Score _scoreGameObject;

    private void ShowScore(int value)
    {
        _scoreText.text = "Score: " + value; 
    }

    private void OnEnable()
    {
        _scoreGameObject.OnScoreChanged += ShowScore;
    }

    private void OnDisable()
    {
        _scoreGameObject.OnScoreChanged -= ShowScore;
    }
}
