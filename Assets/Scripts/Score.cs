using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public event Action<int> OnScoreChanged;

    [SerializeField] private int _score = 0;

    private void AddScore(int value)
    {
        _score = _score + value;

        OnScoreChanged?.Invoke(_score);
    }
    private void OnEnable()
    {
        TargetHitBox.OnHit += AddScore;
    }

    private void OnDisable()
    {
        TargetHitBox.OnHit += AddScore;
    }
}
