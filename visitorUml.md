```plantuml
@startuml

abstract class Hit {
    + Accept(visitor: ITargetVisitor): void
}

class Body {
}

class Head {
}

interface ITargetVisitor {
    + Visit(hit: Body): void
    + Visit(hit: Head): void
}

class RaycastWeapon {
    + ScanHit(hit: Hit): void
}

class TargetHitBox {
    + Visit(hit: Body): void
    + Visit(hit: Head): void
}


Body --|> Hit
Head --|> Hit

Hit <..> ITargetVisitor

RaycastWeapon --> ITargetVisitor

TargetHitBox --> RaycastWeapon

@enduml
